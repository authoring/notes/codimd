CodiMD Deployment
=================

This is a simple Helm chart that deploys CodiMD (with some CERN-specific customizations) on OpenShift.
Two flavours are provided: a standalone server named `codimd-cern` and a CERNBox-integrated appliance named `cernbox-codimd`.
This can also be deployed directly on k8s: just set `enable: false` in the `openshift` key in `values.yaml`.

To deploy the software:

```bash
helm install codimd ./ -f <flavour>.yaml
```

Where `<flavour>` is either `codimd-standalone` or `cernbox-codimd`.

To release a new version of codimd do the following:

```bash
oc login https://<openshift-url> --token=<token>
oc project <desired-project>

helm -n <desired-project> upgrade <flavour> . -f <flavour>.yaml
```

To rollback a faulty release:

```bash
oc login https://<openshift-url> --token=<token>
oc project <desired-project>

helm -n <desired-project> rollback <flavour>
```
